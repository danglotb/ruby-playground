require 'simplecov'
SimpleCov.start

require_relative '../lib/str_char_array'
require "test/unit"

class TestSimpleNumber < Test::Unit::TestCase

  def setup
   @actual = StringCharArray.new('abcd')
  end

  def test_char_at
    assert_equal('a', @actual.charAt(0))
  end

  def test_size
    assert_equal(4, @actual.size)
  end

  def test_to_s
    assert_equal('abcd', @actual.to_s)
  end

end
