class StringCharArray

  attr_reader :chars

  def initialize
    chars = []
  end

  def initialize(string)
    raise "wrong type: string required" unless string.is_a?(String)
    @chars = string.chars
  end

  def charAt(index)
    if index < 0
      return chars[0]
    end
    size = size()
    if index > size()
      return chars[size() - 1]
    end
    return chars[index]
  end

  def size
    return chars.length
  end

  def to_s
    return chars.join('')
  end
end
